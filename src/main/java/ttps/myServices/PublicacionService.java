package ttps.myServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ttps.myPatternDAO.IPublicacionDAO;
import ttps.myPatternDAO.impl.PublicacionDAO;

@Service
public class PublicacionService {

	@Autowired
	private PublicacionDAO publicacion;
	
	public IPublicacionDAO getPublicacionDAO(){
		return publicacion;
	}
	
}
