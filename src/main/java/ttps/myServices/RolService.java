package ttps.myServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import ttps.myClasses.Rol;
import ttps.myPatternDAO.IRolDAO;
import ttps.myPatternDAO.impl.RolDAOImpl;

@Service
public class RolService {
	
	@Autowired
	private RolDAOImpl rolDaoImpl;
	
	public IRolDAO getRolUsuario(){
		return rolDaoImpl;
	}
	
	
	final class StringToRolConverter implements Converter<String, Rol> {
		  public Rol convert(String source) {
		    Rol rol = new Rol();
		    try {
		    	int id = Integer.parseInt(source);
		    	rol = getRolUsuario().getRolByID(id);
		    	return rol;
			} catch (Exception e) {
				return rol;
			}
		    
		  }
		}
	
}
