package ttps.myServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ttps.myPatternDAO.IUsuarioDAO;
import ttps.myPatternDAO.impl.UsuarioDAOImpl;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioDAOImpl usuario;
	
	public IUsuarioDAO getUsuarioDAO(){
		return usuario;
	}
	

}
