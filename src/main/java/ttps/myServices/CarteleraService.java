package ttps.myServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ttps.myPatternDAO.ICarteleraDAO;
import ttps.myPatternDAO.impl.CarteleraDAOImpl;

@Service
public class CarteleraService {
	
	@Autowired
	private CarteleraDAOImpl cartelera;
	
	public ICarteleraDAO getCarteleraDAO(){
		return cartelera;
	}
}
