package ttps.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ttps.myClasses.Cartelera;
import ttps.myClasses.Publicacion;
import ttps.myClasses.Rol;
import ttps.myClasses.Usuario;
import ttps.myFactory.Factory;

@Controller
@RequestMapping("/cartelera")
public class CarteleraController {

	@Autowired
	private Factory factory;
	private static final Logger logger = Logger.getLogger(CarteleraController.class);


	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView listarCarteleras(){
		ModelAndView model = new ModelAndView("carteleraList");

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if ((!auth.isAuthenticated())||(auth.getPrincipal() instanceof String)){
			ModelAndView login = new ModelAndView("redirect:/login");
			return login;
		}
		
		UserDetails userDetail = (UserDetails) auth.getPrincipal();
		logger.info("Usuario: "+userDetail.getUsername());
		List<Cartelera> list = factory.getCarteleraService().getCarteleraDAO().listarCarteleras();

		Usuario usr = factory.getUsuarioService().getUsuarioDAO().findByUserName(userDetail.getUsername());
		if (usr.getRol().getName().equals(Rol.ROLE_ALUMNO)){
			//List<Cartelera> list = usr.getListaCartelera();
			model.addObject("carteleras", list);
		}
		else{
			//TODO hacer las otras redirecciones
			logger.info("Cantidad de carteleras: "+list.size());
			model.addObject("carteleras", list);
		}

		return model;

	}

	@RequestMapping(value="/", method = RequestMethod.POST)
	public String saveOrUpdateCartelera (@ModelAttribute("cartelera") @Validated Cartelera cartelera,
			BindingResult result){
		logger.info("saveOrUpdateCartelera() : {}");

		if (result.hasErrors()) {
			return "redirect:/cartelera/";
		} else {				
			factory.getCarteleraService().getCarteleraDAO().insertOrUpdateCarterlera(cartelera);

			// POST/REDIRECT/GET
			return "redirect:/cartelera/";
		}
	}


	@RequestMapping("/add")
	public ModelAndView agregarCarterlera(){
		ModelAndView add = new ModelAndView("carteleraForm");
		Cartelera cartelera = new Cartelera();
		add.addObject("cartelera",cartelera);
		return add;

	}


	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ModelAndView showUser(@PathVariable("id") int id) {

		ModelAndView showCartelera = new ModelAndView("carteleraForm");
		logger.debug("showCartelera() id: {}");

		Cartelera car = factory.getCarteleraService().getCarteleraDAO().findByID(id);
		if (car == null) {
			showCartelera.addObject("error", "Usuario no encontrado.");		
			return new ModelAndView("redirect:/cartelera/");
		}
		showCartelera.addObject("cartelera",car);

		return showCartelera;

	}

	// delete cartelera
	@RequestMapping(value = "/{id}/delete", method = RequestMethod.GET)
	public String deleteUser(@PathVariable("id") int id,
			final RedirectAttributes redirectAttributes) {

		logger.debug("deleteCartelera() : {}");
		factory.getCarteleraService().getCarteleraDAO().deleteCartelera(id);

		return "redirect:/cartelera/";

	}
	@RequestMapping(value = "/{idCartelera}/publicacion/add")
	public ModelAndView agregarPublicacion(@PathVariable("idCartelera") int id){
		logger.debug("addPublicacion()");
		Cartelera car = factory.getCarteleraService().getCarteleraDAO().findByID(id);
		if (car == null){
			return new ModelAndView("redirect:/cartelera/");
		}
		ModelAndView add = new ModelAndView("publicacionForm");
		Publicacion pub = new Publicacion();
		add.addObject("idCartelera",id);
		add.addObject("publicacion",pub);
		return add;
	}




	@RequestMapping(value = "/{idCartelera}/publicacion/{idPublicacion}")
	public ModelAndView publicacionByCartelera(@PathVariable("idCartelera") int id,
			@PathVariable("idPublicacion") int idPublicacion){
		ModelAndView publicacion = new ModelAndView("publicacionForm");
		logger.debug("showPublicaciones()");
		Cartelera car = factory.getCarteleraService().getCarteleraDAO().findByID(id);
		if (car == null){
			return new ModelAndView("redirect:/cartelera/");
		}

		Publicacion pub = this.carteleraContainsPublicacion(car, idPublicacion);
		
		if (pub == null){
			return new ModelAndView("redirect:/cartelera/");
		}
		publicacion.addObject("publicacion",pub);
		return publicacion;
	}

	@RequestMapping(value="/{idCartelera}/publicacion", method = RequestMethod.POST)
	public String saveOrUpdatePublicacion(@PathVariable("idCartelera") int id,
			@ModelAttribute("publicacion") @Validated Publicacion publicacion,
			BindingResult result){
		logger.info("saveOrUpdatePublicacion() : {}");

		if (result.hasErrors()) {
			return "redirect:/cartelera/";
		} else {				
			Cartelera car = factory.getCarteleraService().getCarteleraDAO().findByID(id);
			if (car == null){
				return "redirect:/cartelera/";
			}
			
			int index = car.getListaPublicacion().indexOf(publicacion);
			if (index < 0){
				car.getListaPublicacion().add(publicacion);
			}else{
				car.getListaPublicacion().set(index, publicacion);
			}
			
			factory.getCarteleraService().getCarteleraDAO().insertOrUpdateCarterlera(car);

			// POST/REDIRECT/GET
			return "redirect:/cartelera/";
		}
	}
	
	//Se supone que una cartelera no va a tener ni siquiera 100 elementos.
	//Sino hay que hacer un DAO para obtener las publicaciones
	//a partir de una cartelera
	private Publicacion carteleraContainsPublicacion(Cartelera car, int idPublicacion){
		Publicacion pub = null;
		for (Publicacion p : car.getListaPublicacion()){
			if (p.getId() == idPublicacion){
				pub = p;
				break;
			}
		}
		return pub;
	}
	
	

}
