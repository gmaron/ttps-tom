package ttps.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ttps.myClasses.Rol;
import ttps.myClasses.Usuario;
import ttps.myFactory.Factory;

@Controller
public class TtpsController {


	private static final Logger logger = Logger.getLogger(TtpsController.class);
	
	@Autowired
	private Factory factory;
	
	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String defaultPage() {
		
		Usuario usr = factory.getUsuarioService().getUsuarioDAO().findByUserName("gmaron");
		
		if (usr == null){
			
			//crear un usuario admin
			Usuario u = new Usuario("Gaston", "Maron", "gaston.maron@gba.gob.ar", "12345", true, "gmaron");
			Rol r1 = new Rol();
			r1.setName(Rol.ROLE_ADMIN);
			u.setRol(r1);
			//factory.getRolService().getRolUsuario().insertRol(r1);
			factory.getUsuarioService().getUsuarioDAO().insertOrUpdateUsuario(u);
			
			//crear un usuario alumno
			Rol r2 = new Rol();
			Usuario u2 = new Usuario("Tomas", "Larruari","tom@gmail.com","qwerty",true,"tom");
			r2.setName(Rol.ROLE_DOCENTE);
			u2.setRol(r2);
			//factory.getRolService().getRolUsuario().insertRol(r2);
			factory.getUsuarioService().getUsuarioDAO().insertOrUpdateUsuario(u2);
			
			
			//crear un usuario docente
			Usuario u3 = new Usuario("Milton", "Meorni", "miltonmeroni@gmail.com", "milton", true, "milton");
			Rol r3 = new Rol();
			r3.setName(Rol.ROLE_ALUMNO);
			u3.setRol(r3);
			//factory.getRolService().getRolUsuario().insertRol(r3);
			factory.getUsuarioService().getUsuarioDAO().insertOrUpdateUsuario(u3);

			
			

			
		}
		
		return "redirect:login";
		

	}

	@RequestMapping(value = "/login", method = {RequestMethod.GET})
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");

		return model;

	}
	
	//for 403 access denied page
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();
		
		//check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			System.out.println(userDetail);
		
			model.addObject("username", userDetail.getUsername());
			
		}
		
		model.setViewName("403");
		return model;

	}
	
	@RequestMapping(value = "/redirect")
	public String redirect (){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			logger.info("Usuario: "+userDetail.getUsername());
			Usuario usr = factory.getUsuarioService().getUsuarioDAO().findByUserName(userDetail.getUsername());
			if (usr.getRol().getName().equals(Rol.ROLE_ADMIN)){
				return "redirect:/admin/users";
			}else{
				//TODO hacer las otras redirecciones
				return "redirect:/cartelera/";
			}
		}
		
		return "redirect:login";
	}
	
	@RequestMapping("/init")
	public String iniciar(){
		
		
		 return "redirect:/login";
	}
	
}
