package ttps.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ttps.myClasses.Rol;
import ttps.myClasses.Usuario;
import ttps.myFactory.Factory;

@Controller
@RequestMapping(value="/admin")
public class AdminController {

	private static final Logger logger = Logger.getLogger(AdminController.class);

	@Autowired
	private Factory factory;
	
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ModelAndView adminPage() {

		ModelAndView model = new ModelAndView("userList");
		List<Usuario> usuarios = this.factory.getUsuarioService().getUsuarioDAO().listUsuarios();
		model.addObject("usuarios",usuarios);

		return model;

	}
	
		// save or update user
		// 1. @ModelAttribute bind form value
		// 2. @Validated form validator
		@RequestMapping(value = "/users", method = RequestMethod.POST)
		public String saveOrUpdateUser(@ModelAttribute("usuario") @Validated Usuario user,
				BindingResult result) {

			logger.info("saveOrUpdateUser() : {}");

			if (result.hasErrors()) {
				return "redirect:/admin/users/";
			} else {				
				
				long id = factory.getUsuarioService().getUsuarioDAO().insertOrUpdateUsuario(user);
				
				// POST/REDIRECT/GET
				return "redirect:/admin/users/" + id;

			}

		}

		// delete user
		@RequestMapping(value = "/users/{id}/delete", method = RequestMethod.POST)
		public String deleteUser(@PathVariable("id") int id,
			final RedirectAttributes redirectAttributes) {

			logger.debug("deleteUser() : {}");
			factory.getUsuarioService().getUsuarioDAO().deleteUsuario(id);

			return "redirect:/admin/users/";

		}
	
		// show user
		@RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
		public ModelAndView showUser(@PathVariable("id") int id) {
				
			ModelAndView showUser = new ModelAndView("userForm");
			logger.debug("showUser() id: {}");

			Usuario user = factory.getUsuarioService().getUsuarioDAO().findByID(id);
			if (user == null) {
				showUser.addObject("error", "Usuario no encontrado.");				
			}
			showUser.addObject("usuario",user);

			return showUser;

		}
	
	@RequestMapping(value="/users/add")
	public ModelAndView add_user(){
		ModelAndView addUser = new ModelAndView("userForm");
		List<Rol> roles = this.factory.getRolService().getRolUsuario().listarRoles();
		addUser.addObject("roles",roles);
		Usuario user = new Usuario();
		addUser.addObject("usuario", user);
		return addUser;
	}
}
