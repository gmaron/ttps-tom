package ttps.myClasses;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name="cartelera")
public class Cartelera {

	@OneToMany (cascade = {CascadeType.MERGE,CascadeType.REMOVE},fetch=FetchType.EAGER) 
	@JoinColumn (name="cartelera_id")
 	private List<Publicacion> listaPublicacion; 
	
	private String titulo;
	
	@Id @GeneratedValue
	private Long id;

	public List<Publicacion> getListaPublicacion() {
		return listaPublicacion;
	}

	public void setListaPublicacion(List<Publicacion> listaPublicacion) {
		this.listaPublicacion = listaPublicacion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	 
	
}
