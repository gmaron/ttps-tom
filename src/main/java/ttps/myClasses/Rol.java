package ttps.myClasses;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "role")
public class Rol {
	
	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String ROLE_ALUMNO = "ROLE_ALUMNO";
	public static final String ROLE_DOCENTE = "ROLE_DOCENTE";
	
    private Long id;
    
    @Column(unique = true) 
    private String name;  
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}