package ttps.myClasses;


import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name="users")
public class Usuario {


	private String nombre;
	private String apellido;
	private String email;
	private String username;
	private String password;
	private boolean enabled;
	
	@Id @GeneratedValue
	private Long id;
	
	@OneToOne(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="rol_id",nullable = false) 
	private Rol rol;

	@ManyToMany
	private List<Cartelera> listaCartelera;
	
	public Usuario(String nombre, String apellido, String email, String password, boolean activo, String nomUsuario) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.password = password;
		this.enabled = activo;
		this.username = nomUsuario;
	}

	public Usuario() {

	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public boolean isActivo() {
		return enabled;
	}

	public void setActivo(boolean activo) {
		this.enabled = activo;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Cartelera> getListaCartelera() {
		return listaCartelera;
	}

	public void setListaCartelera(List<Cartelera> listaCartelera) {
		this.listaCartelera = listaCartelera;
	}

	
}
