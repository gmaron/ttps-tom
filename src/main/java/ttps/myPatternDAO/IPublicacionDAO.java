package ttps.myPatternDAO;

import ttps.myClasses.Publicacion;


public interface IPublicacionDAO {
	public long insertarPublicacion(Publicacion pub);
	public Publicacion updatePublicacion(Publicacion pub);
	public void deletePublicacion(long id);
	public Publicacion findByID(long id);
}
