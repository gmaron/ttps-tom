package ttps.myPatternDAO;

import java.util.List;

import ttps.myClasses.Cartelera;
import ttps.myClasses.Publicacion;

public interface ICarteleraDAO {
	public List<Cartelera> listarCarteleras();
	public List<Publicacion> listarPublicacionesPorCartelera(long id);
	public long insertOrUpdateCarterlera(Cartelera car);
	public void deleteCartelera(long id);
	public Cartelera findByID(long id);

}
