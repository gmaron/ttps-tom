package ttps.myPatternDAO.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ttps.myClasses.Rol;
import ttps.myPatternDAO.IRolDAO;
import ttps.util.HibernateUtil;

@Repository
@Transactional
public class RolDAOImpl implements IRolDAO {

	@Autowired
	private HibernateUtil hibernateUtil;
	
	public long insertRol(Rol rol) {
		return (Long) hibernateUtil.create(rol);
	}

	@Override
	public List<Rol> listarRoles() {
		return hibernateUtil.fetchAll(Rol.class);
	}

	@Override
	public Rol getRolByID(long id) {
		return hibernateUtil.fetchById(id, Rol.class);
	}
	
	

}
