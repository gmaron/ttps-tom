package ttps.myPatternDAO.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ttps.myClasses.Publicacion;
import ttps.myPatternDAO.IPublicacionDAO;
import ttps.util.HibernateUtil;
@Repository
@Transactional
public class PublicacionDAO implements IPublicacionDAO {

	@Autowired
	private HibernateUtil hibernateUtil;
	
	@Override
	public long insertarPublicacion(Publicacion pub) {
		return (long) hibernateUtil.create(pub);
	}

	@Override
	public Publicacion updatePublicacion(Publicacion pub) {
		return (Publicacion) hibernateUtil.update(pub);
	}

	@Override
	public void deletePublicacion(long id) {
		Publicacion p = new Publicacion();
		p.setId(id);
		hibernateUtil.delete(p);
	}

	@Override
	public Publicacion findByID(long id) {
		return (Publicacion) hibernateUtil.fetchById(id, Publicacion.class);
	}

}
