package ttps.myPatternDAO.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ttps.myClasses.Cartelera;
import ttps.myClasses.Usuario;
import ttps.myPatternDAO.IUsuarioDAO;
import ttps.util.HibernateUtil;

@Repository
@Transactional
public class UsuarioDAOImpl implements IUsuarioDAO{

	@Autowired
	private HibernateUtil hibernateUtil;

	@Override
	public long insertOrUpdateUsuario(Usuario usr) {
		
		if (usr.getId() == null){
			return (Long) hibernateUtil.create(usr);
		}
		
		if (this.findByID(usr.getId()) == null){
			return (Long) hibernateUtil.create(usr);
		}else{
			hibernateUtil.merge(usr);
			return usr.getId();
		}
	
	}

	@Override
	public void deleteUsuario(long id) {
		Usuario usr = new Usuario();
		usr.setId(id);
		hibernateUtil.delete(usr);
	}

	@Override
	public List<Usuario> listUsuarios() {
		return hibernateUtil.fetchAll(Usuario.class);
	}

	@Override
	public Usuario findByID(long id) {
		return hibernateUtil.fetchById(id, Usuario.class);
	}

	@Override
	public Usuario findByUserName(String usrName) {
		return hibernateUtil.fectchByField("username", usrName, Usuario.class);
	}

	@Override
	public List<Cartelera> listarCarteleras(long id) {
		Usuario u = this.findByID(id);
		return null; //u.getListaCartelera();
	}

}
