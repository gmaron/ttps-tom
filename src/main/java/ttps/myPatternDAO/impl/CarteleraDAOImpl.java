package ttps.myPatternDAO.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ttps.myClasses.Cartelera;
import ttps.myClasses.Publicacion;
import ttps.myPatternDAO.ICarteleraDAO;
import ttps.util.HibernateUtil;
@Repository
@Transactional
public class CarteleraDAOImpl implements ICarteleraDAO {

	@Autowired
	private HibernateUtil hibernateUtil;
	

	
	@Override
	public List<Cartelera> listarCarteleras() {
		return hibernateUtil.fetchAll(Cartelera.class);
	}

	@Override
	public List<Publicacion> listarPublicacionesPorCartelera(long id) {
		Cartelera c = this.findByID(id);
		return c.getListaPublicacion();
	}

	@Override
	public long insertOrUpdateCarterlera(Cartelera car) {
		
		if(car.getId() == null){
			return (Long) hibernateUtil.create(car);
		}
		
		if(this.findByID(car.getId()) == null){
			return (Long) hibernateUtil.create(car);
		}else{
			hibernateUtil.merge(car);
			return (Long) car.getId();
		}
	}

	@Override
	public void deleteCartelera(long id) {
		Cartelera car = new Cartelera();
		car.setId(id);
		hibernateUtil.delete(car);
	}

	@Override
	public Cartelera findByID(long id) {
		return  (Cartelera) hibernateUtil.fetchById(id,Cartelera.class);
	}

}
