package ttps.myPatternDAO;

import java.util.List;

import ttps.myClasses.Cartelera;
import ttps.myClasses.Usuario;

public interface IUsuarioDAO {

	public long insertOrUpdateUsuario(Usuario usr);
	public void deleteUsuario(long id);
	public List<Usuario> listUsuarios();
	public Usuario findByID(long id);
	public Usuario findByUserName(String usrName);
	public List<Cartelera> listarCarteleras(long id);
}
