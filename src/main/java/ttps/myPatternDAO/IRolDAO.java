package ttps.myPatternDAO;

import java.util.List;

import ttps.myClasses.Rol;

public interface IRolDAO {
	public long insertRol(Rol rol);
	public List<Rol> listarRoles ();
	public Rol getRolByID(long id);
}
