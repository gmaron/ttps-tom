package ttps.myFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ttps.myServices.CarteleraService;
import ttps.myServices.RolService;
import ttps.myServices.UsuarioService;

@Service
public class Factory {
	
	@Autowired
	private UsuarioService usrService;
	
	@Autowired
	private RolService rolService;
	
	@Autowired
	private CarteleraService carteleraService;
	
	public UsuarioService getUsuarioService(){
		return usrService;
	}
	
	public RolService getRolService(){
		return rolService; 
	}
	
	public CarteleraService getCarteleraService(){
		return carteleraService;
	}
	
}
