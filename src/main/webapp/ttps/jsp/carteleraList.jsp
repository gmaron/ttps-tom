<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cartelera</title>


</head>
<body>
	<%@ include file="navbar.jsp"%>
	<script>
		var base = "${pageContext.request.contextPath}";
	</script>
	<link rel="stylesheet" href="${ base }/ttps/css/owl.carousel.min.css">
	<link rel="stylesheet"
		href="${ base }/ttps/css/owl.theme.default.min.css">
	<script src="https://use.fontawesome.com/9f7a3bc6ec.js"></script>


	<link rel="stylesheet" href="${ base }/ttps/css/cartelera.css">

	<script src="${ base }/ttps/js/jquery.min.js"></script>
	<script src="${ base }/ttps/js/owl.carousel.js"></script>
	<div class="container" style="margin-top: 30px;">
		<h1>Carteleras</h1>
		<!-- INICIO CAROUSEL-->
		<div class="row">
			<div class="owl-carousel owl-theme">
				<!-- INICIO CARTELERAS -->
				<c:forEach items="${carteleras}" var="cartelera">
					<spring:url value="/cartelera/${cartelera.id}/delete"
						var="deleteUrl" />
					<spring:url value="/cartelera/${cartelera.id}" var="updateUrl" />
					<div class="item">
						<div class="panel panel-primary">
							<div class="panel-heading" style="">
								"${ cartelera.titulo }"
								<security:authorize
									access="hasAnyRole('ROLE_ADMIN', 'ROLE_DOCENTE')">
									<td style="width: 20%; float: right;"><a
										href="${updateUrl}" class="table-link"> <span
											class="fa-stack"> <i class="fa fa-square fa-stack-2x"></i>
												<i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
										</span>
									</a> <a href="${deleteUrl}" class="table-link danger"> <span
											class="fa-stack"> <i class="fa fa-square fa-stack-2x"></i>
												<i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
										</span>
									</a></td>
								</security:authorize>
							</div>
							<div class="panel-body"
								style="padding-left: -0px; padding-right: 0px;">
								<ul class="timeline ">
									<c:forEach items="${ cartelera.listaPublicacion }"
										var="publicacion">
										<li>
											<div class="timeline-panel">
												<div class="timeline-heading">
													<h4 class="timeline-title">
														"${ publicacion.titulo }"
														<security:authorize
															access="hasAnyRole('ROLE_ADMIN', 'ROLE_DOCENTE')">
															<spring:url
																value="/cartelera/${cartelera.id}/publicacion/${publicacion.id}"
																var="updatePublicacion" />
															<spring:url
																value="/cartelera/${cartelera.id}/publicacion/${publicacion.id}/delete"
																var="deletePublicacion" />
															<td style="width: 20%; float: right;"><a
																href="${ updatePublicacion }" class="table-link"> <span
																	class="fa-stack"> <i
																		class="fa fa-square fa-stack-2x"></i> <i
																		class="fa fa-pencil fa-stack-1x fa-inverse"></i>
																</span>
															</a> <a href="${ deletePublicacion }"
																class="table-link danger"> <span class="fa-stack">
																		<i class="fa fa-square fa-stack-2x"></i> <i
																		class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
																</span>
															</a></td>
														</security:authorize>
													</h4>
												</div>
												<div class="timeline-body">
													<p>"${ publicacion.texto }"</p>
												</div>
											</div>
										</li>
									</c:forEach>
								</ul>
								<spring:url value="/cartelera/${cartelera.id}/publicacion/add"
									var="addPublicacion" />
								<div class="form-group">
									<button id="singlebutton" name="singlebutton"
										class="btn btn-primary"
										onclick="window.location.href='${addPublicacion}'"
										style="width: 80%; margin-left: 36px; margin-top: 10px;">Nueva
										Publicación</button>
								</div>
							</div>

						</div>
					</div>
				</c:forEach>

			</div>
		</div>

	</div>
	<script>
		$(document).ready(function() {
			var owl = $('.owl-carousel');
			owl.owlCarousel({
				items : 3,
				loop : true,
				margin : 10,
				autoplay : true,
				autoplayTimeout : 3000,
				autoplayHoverPause : true,
				dots : true,
				pagination : true
			});
			$('.play').on('click', function() {
				owl.trigger('play.owl.autoplay', [ 5000 ])
			})
			$('.stop').on('click', function() {
				owl.trigger('stop.owl.autoplay')
			})
		})
	</script>

	<!-- scripts -->
	<script src="${ base }/ttps/js/highlight.js"></script>
	<script src="${ base }/ttps/js/app.js"></script>

</body>
</html>