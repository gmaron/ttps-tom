<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Publicacion</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
	integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
	integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
	integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
	integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
	crossorigin="anonymous"></script>
</head>
<body style="background-color: rgba(204, 204, 204, 0.14)">

	<div class="container">
		<spring:url value="/cartelera/${ idCartelera }/publicacion" var="publicacionAction" />

		<form:form class="form-horizontal col-md-6"
			style="background-color: white; margin: auto;" method="post"
			modelAttribute="publicacion" action="${publicacionAction}">

			<form:hidden path="id" />
			<fieldset>

				<!-- Form Name -->
				<c:choose>
					<c:when test="${empty publicacion['id']}">
						<legend>Crear publicación</legend>
					</c:when>
					<c:otherwise>
						<legend>Actualizar publicación</legend>
					</c:otherwise>
				</c:choose>
				<spring:bind path="titulo">
					<!-- Text input-->
					<div class="form-group">
						<label class=" control-label" for="textinput">Título</label>
						<div class="">
							<input id="titulo" name="titulo" value="${ publicacion.titulo }"
								type="text" placeholder="" class="form-control input-md">
						</div>
					</div>
				</spring:bind>

				<spring:bind path="texto"></spring:bind>
				<div class="form-group">
					<label class=" control-label" for="textarea">Contenido</label>
					<div class="">
						<textarea class="form-control" value="${ publicacion.texto }"
							id="texto" name="texto"></textarea>
					</div>
				</div>
				<c:choose>
					<c:when test="${empty publicacion['id']}">
						<button type="submit" class="btn btn-primary" class="form-group"
							name="singlebutton">Agregar</button>
					</c:when>
					<c:otherwise>
						<button type="submit" class="btn btn-primary" class="form-group"
							name="singlebutton">Actualizar</button>
					</c:otherwise>
				</c:choose>

			</fieldset>

		</form:form>

	</div>
</body>
</html>