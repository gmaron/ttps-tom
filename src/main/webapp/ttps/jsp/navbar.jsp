<!DOCTYPE html>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link
	href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css"
	rel="stylesheet">
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">TTPS JAVA</a>
		</div>
		<ul class="nav navbar-nav">
		
			<security:authorize access="hasRole('ROLE_ADMIN')" var="isAdmin">
				<spring:url value="/admin/users/add" var="altaUsuarioAction" />
				<spring:url value="/admin/users" var="listarUsuarioAction" />
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">Usuarios <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="${ altaUsuarioAction }">Alta</a></li>
						<li><a href="${ listarUsuarioAction }">Listado</a></li>
					</ul></li>
			</security:authorize>

			<security:authorize access="hasAnyRole('ADMIN', 'DOCENTE')">
				<spring:url value="/cartelera/" var="listarCarteleraAction" />
				<spring:url value="/cartelera/add" var="altaCarteleraAction" />
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">Cartelera <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="${ altaCarteleraAction }">Alta</a></li>
						<li><a href="${ listarCarteleraAction }">Listar</a></li>
					</ul></li>
			</security:authorize>

		</ul>
		<ul class="nav navbar-nav navbar-right">
			<c:if test="${pageContext.request.userPrincipal.name != null}">
				<li><a href="#"><span class="glyphicon glyphicon-user"></span>
						Bienvenido @${pageContext.request.userPrincipal.name} ! </a></li>
				<li><a href="javascript:formSubmit()"><span
						class="glyphicon glyphicon-log-out"></span> Logout</a></li>

			</c:if>
		</ul>
	</div>
</nav>

<c:url value="/j_spring_security_logout" var="logoutUrl" />

<!-- csrt for log out-->
<form action="${logoutUrl}" method="post" id="logoutForm">
	<input type="hidden" name="${_csrf.parameterName}"
		value="${_csrf.token}" />
</form>

<script>
	function formSubmit() {
		document.getElementById("logoutForm").submit();
	}
</script>

