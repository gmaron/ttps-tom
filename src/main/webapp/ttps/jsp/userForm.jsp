<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>

<head>
<title></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body style="background-color: rgba(204, 204, 204, 0.14)">
	<%@ include file="navbar.jsp"%>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
		integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
		integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
		crossorigin="anonymous"></script>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

	<div class="container">

		<spring:url value="/admin/users" var="userActionUrl" />
		<div class="row">
			<form:form class="form-horizontal col-md-6"
				style="background-color: white; margin: auto;" method="post"
				modelAttribute="usuario" action="${userActionUrl}">
				<form:hidden path="id" />


				<!-- Form Name -->
				<c:choose>
					<c:when test="${empty usuario['id']}">
						<legend>Crear cuenta</legend>
					</c:when>
					<c:otherwise>
						<legend>Actualizar cuenta</legend>
					</c:otherwise>
				</c:choose>

				<!-- Text input-->
				<spring:bind path="nombre">
					<div class="form-group">
						<label class=" control-label" for="nombre">Nombre</label>
						<div class="">
							<input id="nombre" name="nombre" value="${ usuario.nombre }"
								type="text" placeholder="" class="form-control input-md">

						</div>
					</div>
				</spring:bind>

				<!-- Text input-->
				<spring:bind path="apellido">
					<div class="form-group">
						<label class=" control-label" for="apellido">Apellido</label>
						<div class="">
							<input id="apellido" name="apellido"
								value="${ usuario.apellido }" type="text" placeholder=""
								class="form-control input-md">

						</div>
					</div>
				</spring:bind>

				<spring:bind path="email">
					<!-- Text input-->
					<div class="form-group">
						<label class=" control-label" for="email">Email</label>
						<div class="">
							<input id="email" name="email" value="${ usuario.email }"
								type="email" placeholder="" class="form-control input-md">

						</div>
					</div>
				</spring:bind>

				<spring:bind path="username">
					<!-- Text input-->
					<div class="form-group">
						<label class=" control-label" for="username">Nombre
							usuario</label>
						<div class="">
							<input id="username" name="username"
								value="${ usuario.username }" type="text" placeholder=""
								class="form-control input-md">

						</div>
					</div>
				</spring:bind>

				<spring:bind path="password">
					<!-- Password input-->
					<div class="form-group">
						<label class=" control-label" for="password">Password</label>
						<div class="">
							<input id="password" value="${ usuario.password }"
								name="password" type="password" placeholder=""
								class="form-control input-md">

						</div>
					</div>
				</spring:bind>

				<spring:bind path="rol">
					<!-- Select Basic -->
					<div class="form-group">
						<label class=" control-label" for="selectbasic">Rol</label>
						<div class="">
							<c:choose>
								<c:when test="${empty usuario['id']}">
									<select id="rol" name="rol" class="form-control">
										<c:forEach items="${roles}" var="rol">
											<c:set var="string1" value="${rol.name}" />
											<c:set var="string2" value="${fn:substring(string1, 5,15)}" />
											<c:set var="string3" value="${fn:toLowerCase(string2)}" />
											<option value="${ rol }">${ string3 }</option>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
									<select disabled="true" id="rol" name="rol"
										class="form-control">
										<c:set var="string1" value="${usuario.rol.name}" />
										<c:set var="string2" value="${fn:substring(string1, 5,15)}" />
										<c:set var="string3" value="${fn:toLowerCase(string2)}" />
										<option value="${ usuario.rol.id }">${ string3 }</option>

									</select>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</spring:bind>
				<br>
				<c:choose>
					<c:when test="${empty usuario['id']}">
						<button type="submit" class="btn btn-primary" class="form-group"
							name="singlebutton">Agregar</button>
					</c:when>
					<c:otherwise>
						<button type="submit" class="btn btn-primary" class="form-group"
							name="singlebutton">Actualizar</button>
					</c:otherwise>
				</c:choose>

			</form:form>

		</div>
	</div>
</body>


</html>