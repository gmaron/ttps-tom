<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<!--  This file has been downloaded from bootdey.com    @bootdey on twitter -->
<!--  All snippets are MIT license http://bootdey.com/license -->
<!-- 
    	The codes are free, but we require linking to our web site.
    	Why to Link?
    	A true story: one girl didn't set a link and had no decent date for two years, and another guy set a link and got a top ranking in Google! 
    	Where to Put the Link?
    	home, about, credits... or in a good page that you want
    	THANK YOU MY FRIEND!
    -->
<title>Listado usuarios</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<script src="https://use.fontawesome.com/9f7a3bc6ec.js"></script>

<script>var base = "${pageContext.request.contextPath}";</script>
<link rel="stylesheet" href="${ base }/ttps/css/userList.css">

</head>
<body>
	<%@ include file="navbar.jsp"%>
	<hr>
	<div class="container bootstrap snippet">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-box no-header clearfix">
					<div class="main-box-body clearfix">
						<div class="table-responsive">
							<table class="table user-list">
								<thead>
									<tr>
										<th><span>Usuario</span></th>
										<th><span>Email</span></th>
										<th>Operaciones</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${usuarios}" var="usuario">
									<spring:url value="/admin/users/${usuario.id}"
												var="updateUrl" />
										<tr>
											<td><img src="http://bootdey.com/img/Content/user_1.jpg"
												alt=""> <a href="${updateUrl}" class="user-link">${ usuario.nombre }
													${ usuario.apellido }</a> <span class="user-subhead"> <c:set
														var="string1" value="${usuario.rol.name}" /> <c:set
														var="string2" value="${fn:substring(string1, 5,15)}" /> <c:set
														var="string3" value="${fn:toLowerCase(string2)}" />
													${string2}
											</span></td>
											<td><a href="mailto:${ usuario.email }"> ${ usuario.email }
											</a></td>
											<spring:url value="/admin/users/${usuario.id}/delete"
												var="deleteUrl" />
											
											<td style="width: 20%;"><a href="${updateUrl}"
												class="table-link"> <span class="fa-stack"> <i
														class="fa fa-square fa-stack-2x"></i> <i
														class="fa fa-pencil fa-stack-1x fa-inverse"></i>
												</span>
											</a> <a href="${deleteUrl}'" class="table-link danger"> <span
													class="fa-stack"> <i
														class="fa fa-square fa-stack-2x"></i> <i
														class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
												</span>
											</a></td>

										</tr>

									</c:forEach>
									<!--- FIN USUARIO 1-->
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:url value="/j_spring_security_logout" var="logoutUrl" />

	<!-- csrt for log out-->
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>

	<script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>

</body>
</html>