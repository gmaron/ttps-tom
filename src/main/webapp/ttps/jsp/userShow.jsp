<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<title></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body style="background-color: rgba(204, 204, 204, 0.14)">
	<div class="container">
		<%@ include file="navbar.jsp"%>

		<!-- Form Name -->

		<legend>Detalles del usuario</legend>

		<!-- Text input-->
		<div class="form-group">
			<label class=" control-label" for="textinput">Nombre</label>
			<div class="">
				<input id="textinput" name="textinput" value="${ usuario.nombre }"
					type="text" placeholder="" class="form-control input-md">

			</div>
		</div>

		<div class="form-group">
			<label class=" control-label" for="textinput">Apellido</label>
			<div class="">
				<input id="textinput" name="textinput" value="${ usuario.apellido }"
					type="text" placeholder="" class="form-control input-md">

			</div>
		</div>

		<!-- Text input-->
		<div class="form-group">
			<label class=" control-label" for="textinput">Email</label>
			<div class="">
				<input id="textinput" name="textinput" value="${ usuario.email }"
					type="email" placeholder="" class="form-control input-md">

			</div>
		</div>

		<!-- Text input-->
		<div class="form-group">
			<label class=" control-label" for="textinput">Nombre usuario</label>
			<div class="">
				<input id="textinput" name="textinput" value="${ usuario.username }"
					type="text" placeholder="" class="form-control input-md">

			</div>
		</div>

		<!-- Password input-->
		<div class="form-group">
			<label class=" control-label" for="passwordinput">Password</label>
			<div class="">
				<input value="${ usuario.password }" id="passwordinput"
					name="passwordinput" type="password" class="form-control input-md">

			</div>
		</div>

		<!-- Select Basic -->
		<div class="form-group">
			<label class=" control-label" for="selectbasic">Rol</label>
			<div class="">
				<select id="selectbasic" name="selectbasic" class="form-control">
					<c:set var="string1" value="${usuario.rol.name}" />
					<c:set var="string2" value="${fn:substring(string1, 5,15)}" />
					<c:set var="string3" value="${fn:toLowerCase(string2)}" />
					<option value="${ usuario.rol.id }">${ string3 }</option>
				</select>
			</div>
		</div>

		<!-- Button -->
		<div class="form-group">
			<button id="singlebutton" name="singlebutton" class="btn btn-primary">Guardar</button>
		</div>
	</div>
</body>
</html>