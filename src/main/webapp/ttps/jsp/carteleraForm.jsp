<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="es">
<head>
<title></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body style="background-color: rgba(204, 204, 204, 0.14)">
	<%@ include file="navbar.jsp"%>

	<div class="container">
			<spring:url value="/cartelera/" var="carteleraAction" />
	
		<form:form class="form-horizontal col-md-6"
			style="background-color: white; margin: auto;"
			modelAttribute="cartelera"
			action="${ carteleraAction }"
			method="POST">
			<fieldset>
			<form:hidden path="id" />

				<!-- Form Name -->
				<c:choose>
					<c:when test="${empty cartelera['id']}">
						<legend>Crear cartelera</legend>
					</c:when>
					<c:otherwise>
						<legend>Actualizar cartelera</legend>
					</c:otherwise>
				</c:choose>

				<!-- Text input-->
				<spring:bind path="titulo">
					<div class="form-group">
						<label class=" control-label" for="titulo">T�tulo</label>
						<div class="">
							<input id="titulo" value="${ cartelera.titulo }" name="titulo" type="text" placeholder=""
								class="form-control input-md">
						</div>
					</div>
				</spring:bind>
				<!-- Button -->
				<c:choose>
					<c:when test="${empty cartelera['id']}">
						<div class="form-group">
							<button id="singlebutton" name="singlebutton"
								class="btn btn-primary">Guardar</button>
						</div>
					</c:when>
					<c:otherwise>
						<div class="form-group">
							<button id="singlebutton" name="singlebutton"
								class="btn btn-primary">Actualizar</button>
						</div>
					</c:otherwise>
				</c:choose>

			</fieldset>
		</form:form>

	</div>

</body>
</html>
